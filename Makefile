all:
	pandoc index.md -c main.css -f markdown -t html5 --email-obfuscation=javascript -H header.html --include-after-body after.html --include-before-body before.html -S --normalize --metadata=title:'OcLaunch: Launch one by one' -o index.html

deploy-fast: all
	scp -r index.html leowzukw@ssh.tuxfamily.org:oclaunch/o.oclaunch.eu.org-web/htdocs

deploy: all
	scp -r index.html favicon.ico favicon.png pure-combo.css main.css jquery.cookies.js jquery.socialshareprivacy.min.autoload.js jquery.min.js .htaccess ./SocialSharePrivacy/ leowzukw@ssh.tuxfamily.org:oclaunch/o.oclaunch.eu.org-web/htdocs

